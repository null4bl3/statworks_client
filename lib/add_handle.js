let chalk = require('chalk');
const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

/**
 * add handle
 *
 * @param {*} step
 *
 * @returns Promise.resolve()
 */
module.exports = (step) => {
  switch (step) {
    case 1:
      return new Promise((resolve, reject) => {
        let _try = function() {
          rl.question(chalk.yellow('\nClient Token: '), (answer) => {
            if (answer) {
              resolve(answer);
            } else {
              console.log(chalk.blue(" - Supply the client token obtained at https://statworks.eu \n "));
              _try();
            }
          });
        };
        _try();
      });
      break;
    case 2:
      return new Promise((resolve, reject) => {
        let _try = function() {
          rl.question(chalk.yellow('\nAccount Key: '), (answer) => {
            if (answer) {
              resolve(answer);
            } else {
              console.log(chalk.blue(" - Supply the Account Key obtained at https://statworks.eu \n "));
              _try();
            }
          });
        };
        _try();
      });
      break;
    case 3:
      return new Promise((resolve, reject) => {
        console.log(chalk.blue('\nAuthorizing .. \n'));
        resolve();
      });
      break;
  }
}