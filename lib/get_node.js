const {
  spawn
} = require('child_process');
let node_found = false;
let exec_path = "";

/**
 * get_node
 * 
 * @returns exec_path
 */
module.exports = new Promise((resolve, reject) => {
  const node_path = spawn('which', ['node']);
  node_path.stdout.on('data', (data) => {
    exec_path = data.toString();
    node_found = true;
  });

  node_path.on('close', (code) => {
    if (code) {
      node_found = false;
      console.log("CODE", code);
      if (!node_found) {
        const nodejs_path = spawn('which', ['nodejs']);
        nodejs_path.stdout.on('data', (data) => {
          exec_path = data.toString();
        });
        nodejs_path.on('close', (code) => {
          reject(" - Unable to find nodejs installed on your system -");
        });
      } else {
        resolve(exec_path.trim());
      }
    } else {
      exec_path;
      resolve(exec_path.trim());
    }
  });
});