let shell = require('shelljs');

/**
 * df_list
 * 
 * @returns df_list[]
 */
module.exports = () => {
  return new Promise((resolve) => {
    let df_list = [];
    let tmp = shell.exec("df", {
      silent: true
    });
    tmp = tmp.split("\n");
    tmp.shift();
    tmp.forEach((item, index) => {
      let tmp_list = item.split(/(\s+)/).filter((e) => {
        return e.trim().length > 0;
      });
      if (tmp_list[0]) {
        df_list.push({
          filesystem: tmp_list[0],
          size: tmp_list[1],
          used: tmp_list[2],
          available: tmp_list[3],
          procentage: tmp_list[4],
          mountpoint: tmp_list[5]
        });
      } else {
        resolve(df_list);
      }
    });
  });
}