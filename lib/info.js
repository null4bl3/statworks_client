let moment = require('moment');
let col = require('colors');

/**
 * info
 * 
 * start function to display app info
 * 
 * @returns the nothingness
 */
module.exports = () => {
  console.log();
  for (var i = 0; i < process.stdout.columns; i++) {
    process.stdout.write(col.cyan("="));
  }
  require('./term_line.js')(' ');
  require('./term_line.js')(moment().format("YYYY/MM/DD - HH:mm:ss"));
  require('./bnnr').forEach((el) => {
    for (let d = 0; d < center(el); d++) {
      process.stdout.write(" ");
    }
    console.log(el);
  });
  require('./term_line')("------------------------");
  require('./term_line')("» https://statworks.eu «");
  require('./term_line')("------------------------");
  for (var i = 0; i < process.stdout.columns; i++) {
    process.stdout.write(col.cyan("="));
  }
  console.log();
}

let center = function(_string) {
  return process.stdout.columns / 2 - _string.length / 2;
};