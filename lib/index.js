module.exports = {
  banner: require('./bnnr'),
  df_list: require('./df_list'),
  data: require('./data_store'),
  uptime: require('./uptime'),
  ids: require('./ids'),
  maid: require('./manual_id'),
  clog: require('./clog'),
  auth_account: require('./auth_account'),
  info: require('./info'),
  add_handle: require('./add_handle'),
  line: require('./term_line')
};