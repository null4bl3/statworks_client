let moment = require("moment");
let fs = require("fs");
let lib = require("./index");
let json_store = require("json-store");
let db = json_store(process.env.PATH + "/config.json");
let chalk = require("chalk");
const util = require("util");
const exec = util.promisify(require("child_process").exec);
const { spawn } = require("child_process");
let ps = require("ps-node");
const { promisify } = require("util");
let lookup = promisify(ps.lookup);
let os = require("os");
let sys_stats = {};
let col = require("colors");
const path = require("path");
let http = require("http");
const shell = require("shelljs");
const got = require("got");
const sys = require("systeminformation");
const FormData = require("form-data");
let banner = require("./bnnr.js");
const readline = require("readline");
let _ = require("lodash");
let response_time_list = [];
let ID = require("./manual_id");
let Writable = require("stream").Writable;
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});
let missing_procs = [];
let notifiers = [];
let watch_object = {};

let time_trigger = 3600000;

let base_url = "";
if (process.env.NODE_ENV === "dev") {
  base_url = "localhost:9000";
  process.env.BASE_URL = "localhost:9000";
  time_trigger = 60000;
} else {
  base_url = "https://statworks.eu";
  process.env.BASE_URL = "https://statworks.eu";
}

if (process.env.NODE_ENV === "DEBUG") {
  time_trigger = 60000;
}

/**
 *  GO RUN
 */
(async () => {
  let key = db.get("key");
  let token = db.get("token");

  let hash = db.get("hash");
  let hostname = db.get("hostname");
  let ip = db.get("ip");
  let mac = db.get("mac");

  let watchers = db.get("watchers");

  lib.clog.test();
  lib.info();
  lib.clog.start();
  let client;
  let data;
  try {
    client = await lib.ids();
    if (!key || !token) {
      return add_client();
    } else {
      if (!hash || !hostname || !ip || !mac) {
        db.set("hash", client.hash);
        db.set("hostname", client.hostname);
        db.set("ip", client.ip);
        db.set("mac", client.mac);
      } else {
        if (hash !== client.hash) {
          lib.clog.error("BAD HASH!");
          reset_db();
        }
        if (!data.status.client_token || !data.status.account_key) {
          add_client();
        } else {
          console.log("[PROC] - RUNNING ..");
          setInterval(() => {
            return status_iteration();
          }, time_trigger);
        }
      }
    }
    if (data.status.watchers && data.status.watchers.length > 0) {
      return watch_iterator();
    }
  } catch (e) {
    lib.clog.error(e);
  }
})();

/**
 * reset_db
 */

let reset_db = () => {
  lib.clog.reset(moment().format("YYYY / MM / DD - HH:mm"));
  db.set("key", "");
  db.set("token", "");
  db.set("hash", "");
  db.set("hostname", "");
  db.set("ip", "");
  db.set("mac", "");
};

/**
 * add_client
 */
let add_client = async () => {
  let tmp = {};
  let token = "";
  let key = "";
  let mac = "";
  let ip = "";
  let hostname = "";
  let client = "";
  let state;

  try {
    token = await lib.add_handle(1);
    key = await lib.add_handle(2);
    state = await lib.add_handle(3);
  } catch (e) {
    lib.clog.error(e);
  }

  try {
    mac = await lib.maid.get_mac_address();
    ip = await lib.maid.ip_get();
    hostname = await lib.maid.hostname();
    lib.clog.auth();
  } catch (e) {
    lib.clog.error(e);
  }

  try {
    client = await lib.auth_account({
      client_token: token,
      account_key: key,
      mac: mac,
      ip: ip,
      hostname: hostname
    });
  } catch (e) {
    lib.clog.auth_failed();
  }

  if (client.statusCode === 200 && client.statusMessage === "OK") {
    lib.clog.req_ok(client.statusCode);
    lib.clog.auth_req(client.statusMessage);
    db.set("key", key);
    db.set("token", token);
    return true;
  } else {
    lib.clog.error(" Not Authorized!");
    console.log("\nNot Authorized! - Check your credentials ..\n");
    return process.exit(0);
  }
};

/**
 * auth_account
 *
 * @param {*} object
 */
let auth_account = (object) => {
  return got.post(base_url + "/api/usr/author", {
    body: {
      acc: object
    },
    json: true
  });
};

/**
 * cmd_handle
 *
 * @param {*} cmd
 */
let cmd_handle = (cmd) => {
  console.log("[PRAM] - ", cmd);
  switch (cmd[0]) {
    case "reset":
      // db.push("/", {});
      reset_db();
      break;
    case "check":
      console.log(cmd[1]);
      break;
  }
};

/**
 * filter_list
 *
 * @param {*} data_list
 */
let filter_list = (data_list) => {
  let proc_list = [];
  let prc_lst = new Map();
  for (const proc of data_list) {
    if (!parseInt(proc.name)) {
      let prc = _.find(proc_list, {
        name: proc.name
      });
      if (prc) {
        let tmp = _.findIndex(proc_list, {
          name: proc.name
        });
        if (proc.pcpu > 0) {
          proc_list[tmp].instances++;
        }
      } else {
        if (proc.pcpu > 0) {
          proc_list.push({
            name: proc.name,
            instances: 1
          });
        }
      }
    }
  }
  return proc_list;
};

/**
 * watch_iterator
 *
 * @param {*} _list
 */
let watch_iterator = async () => {
  if (watch_object) {
    for (let wtch of Object.keys(watch_object)) {
      clearInterval(watch_object[wtch]);
    }
  }
  try {
    let the_watch_list = db.getData("/status/watchers");
    let data = db.getData("/");
    let item_index = 1;
    for (let item of the_watch_list) {
      watch_object[item_index] = setInterval(async () => {
        let state = await check_for_proc(item.name);
        if (!state) {
          if (!missing_procs.includes(item.name)) {
            lib.clog.down(item.name);
            missing_procs.push(item.name);
          }
        } else {
          if (missing_procs.includes(item.name)) {
            missing_procs.slice(missing_procs.indexOf(item.name), 1);
          }
        }
        if (missing_procs.length > 0) {
          send_to_notify(missing_procs);
        }
        item_index++;
      }, item.interval);
    }
  } catch (e) {
    lib.clog.error(e);
  }
};

let rebuild_array = async (_list) => {
  let tmp = [];
  for (let item of _list) {
    if (item) {
      tmp.push(item);
    }
  }
  return tmp;
};

/**
 * check_for_proc
 *
 * @param {*} _proc
 */
let check_for_proc = async (_proc) => {
  return new Promise((resolve, reject) => {
    const ls = spawn("ps", ["-fC", _proc]);
    ls.stdout.on("data", (data) => {
      if (data.toString().includes(_proc)) {
        resolve(1);
      } else {
        resolve(0);
      }
    });
    ls.stderr.on("data", (data) => {
      lib.clog.error(data);
      reject(data);
    });
  });
};

/**
 * send_to_notify
 *
 * @param {*} _list
 */
let send_to_notify = async (_list) => {
  let data = await db.getData("/");
  let tmp_glob = {
    token: data.status.client_token,
    key: data.status.account_key,
    hash: data.status.uniques.hash,
    mac: data.status.uniques.mac
  };
  for (const item of _list) {
    if (!notifiers.includes(item)) {
      lib.clog.notified();
      try {
        let tmp = await got.post(base_url + "/api/Clients/alert", {
          body: {
            alert: {
              ident: tmp_glob,
              alert: item.proc
            }
          },
          json: true
        });
        notifiers.push(item);
      } catch (e) {
        lib.clog.error(e);
      }
    } else {
      return true;
    }
  }
};

/**
 * status_iteration
 */
let status_iteration = async () => {
  let tmp_time = 0;
  let proc_list = [];
  let prc_lst = new Map();
  const data = db.getData("/");
  const form = new FormData();
  true;
  let all_data;
  let res;
  lib.clog.gather();
  try {
    sys_stats.uptime = await lib.uptime();
    sys_stats.fs_stats = await lib.df_list();
    all_data = await sys.getAllData();
    sys_stats.proc_list = await filter_list(all_data.processes.list);
    sys_stats.response_time_list = response_time_list;
    sys_stats.mem = all_data.mem;
    sys_stats.temp = all_data.temp;
    sys_stats.disks_io = all_data.disksIO;
    sys_stats.proc_count = all_data.processes.all;
    sys_stats.time = all_data.time;
    sys_stats.freemem = os.freemem();
    sys_stats.status_uptime = os.uptime();
    sys_stats.current_load = all_data.currentLoad;
    sys_stats.account_key = data.status.account_key;
    sys_stats.client_token = data.status.client_token;
    lib.clog.send(moment().format("YYYY.MM.DD - HH:mm:ss"));
    tmp_time = Date.now();
    res = await got.post(base_url + "/api/Stat/istics", {
      body: {
        data: sys_stats
      },
      json: true
    });
    let count = Date.now() - tmp_time;
    let status = res.body.status;
    if (status && status.watches && status.watches.length > 0) {
      db.set("watchers", status.watches);
    }
    if (response_time_list.length > 23) {
      response_time_list.splice(0, 1);
      response_time_list.push(count);
    } else {
      response_time_list.push(count);
    }
    lib.clog.req_ok(res.statusCode);
    lib.clog.time(count);
    return true;
  } catch (e) {
    lib.clog.error(e);
  }
};
