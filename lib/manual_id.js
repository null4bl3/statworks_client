const publicIp = require('public-ip');
const os = require("os");
const crypto = require('crypto');
const hash = crypto.createHash('sha256');
const Promise = require('bluebird'); // jshint ignore:line
let client_identification = {};
let promise_array = [];

/**
 * manual_ids
 * 
 * export functions for: 
 * get_mac_address
 * write_data_hash
 * hostname
 * ip_get
 * 
 */
module.exports = {

  get_mac_address: function() {
    let tmp_mac = "";
    let mac_add = os.networkInterfaces();
    return new Promise((resolve, reject) => {
      Object.keys(mac_add).forEach(function(key, index) {
        for (var i = 0; i < mac_add[key].length; i++) {
          if (mac_add[key][i].mac !== "00:00:00:00:00:00") {
            tmp_mac = mac_add[key][i].mac;
          }
        }
        if (index === Object.keys(mac_add).length - 1) {
          if (tmp_mac === "00:00:00:00:00:00") {
            resolve("00:00:00:00:00:00");
          } else {
            resolve(tmp_mac);
          }
        }
      });
    });
  },

  write_data_hash: function(data) {
    return new Promise((resolve, reject) => {
      hash.update(JSON.stringify(data));
      var tmp = hash.digest('hex');
      resolve(tmp);
    });
  },

  hostname: function() {
    return new Promise((resolve, reject) => {
      resolve(os.hostname())
    })
  },

  ip_get: function() {
    return new Promise((resolve, reject) => {
      publicIp.v4()
        .then(ip => {
          resolve(ip);
        });
    });
  }
}