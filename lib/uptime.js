let shell = require('shelljs');
/**
 * uptime
 * 
 * runs the linux uptime -s command
 * 
 * @returns results
 */
module.exports = () => {
  return new Promise((resolve, reject) => {
    shell.exec('uptime -s', {
      silent: true
    }, (e, r) => {
      if (e) {
        console.log(e);
        reject(e);
      } else {
        resolve(r.trim());
      }
    });
  });
}