let fs = require("fs");
let data_file = {};

module.exports = {
  init_check: async () => {
    let file_path = process.env.PATH + "/config.json";
    try {
      if (fs.existsSync(process.env.PATH + "/config.json")) {
        data_file = await fs.readFileSync(file_path);
        return JSON.parse(data_file);
      } else {
        let data_file = await fs.writeFileSync(
          file_path,
          JSON.stringify({
            key: "",
            token: ""
          })
        );
        return data_file;
      }
    } catch (e) {
      console.log(e);
    }
  },

  set: () => {},

  get: () => {}
};
