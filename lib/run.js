let get = require('./lib/get_node');

console.log(process.argv[0]);

const {
  spawn
} = require('child_process');
const ls = spawn('./node_modules/forever/bin/forever', ['start', '-a', '-l', 'statworks.log', '-o', 'statworks_out.log', '-e', 'statworks_err.log', './srv.js']);

ls.stdout.on('data', (data) => {
  console.log(`stdout: ${data}`);
  process.exit(0);
});

ls.stderr.on('data', (data) => {
  console.log(`stderr: ${data}`);
});

ls.on('close', (code) => {
  console.log(`child process exited with code ${code}`);
});