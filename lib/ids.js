const os = require("os");
const crypto = require('crypto');
const public_id = require('public-ip');
const hash = crypto.createHash('sha256');
const Promise = require('bluebird'); // jshint ignore:line
let client_identification = {};
let promise_array = [];

/**
 * ids
 * 
 * @returns mac, ip, hash_id
 */
module.exports = function() {

  return get_mac_adress()
    .then((mac_) => {
      client_identification.mac = mac_;
      return public_id.v4();
    })
    .then((_ip) => {
      client_identification.ip = _ip;
      return write_data_hash(client_identification.mac);
    })
    .then((_hash) => {
      client_identification.hash = _hash;
      client_identification.hostname = os.hostname();
      return client_identification;
    })
    .catch((e) => {
      console.log("[FAIL] - Check Your Internet Connection");
    });
};

var get_mac_adress = function() {
  let tmp_mac = "";
  let mac_add = os.networkInterfaces();
  return new Promise((resolve, reject) => {
    Object.keys(mac_add).forEach(function(key, index) {
      for (var i = 0; i < mac_add[key].length; i++) {
        if (mac_add[key][i].mac !== "00:00:00:00:00:00") {
          tmp_mac = mac_add[key][i].mac;
        }
      }
      if (index === Object.keys(mac_add).length - 1) {
        if (tmp_mac === "00:00:00:00:00:00") {
          resolve("00:00:00:00:00:00");
        } else {
          resolve(tmp_mac);
        }
      }
    });
  });
};

var write_data_hash = function(data) {
  return new Promise((resolve, reject) => {
    hash.update(JSON.stringify(data));
    var tmp = hash.digest('hex');
    resolve(tmp);
  });
};



var ipGet = function() {
  return new Promise((resolve, reject) => {
    public_id.v4().then(ip => {
      next(ip);
    });
  });
}