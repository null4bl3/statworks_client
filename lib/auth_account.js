const got = require('got');

/**
 * auth account
 * 
 * @param {*} object 
 * 
 * @returns got.post()
 */
module.exports = (object) => {
  return got.post(process.env.BASE_URL + "/api/usr/author", {
    body: {
      acc: object
    },
    json: true
  });
}