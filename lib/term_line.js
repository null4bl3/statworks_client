/**
 * term_line
 * 
 * global term line function for tty based terminals
 * 
 * @param {*} value 
 */
module.exports = function(value) {
  var center = function(_string) {
    return process.stdout.columns / 2 - _string.length / 2;
  };
  for (var d = 0; d < center(value); d++) {
    process.stdout.write(" ");
  }
  console.log(value);
}