const winston = require("winston");
let moment = require("moment");
/**
 * single point file logs
 */

const levels = {
  error: 0,
  warn: 1,
  info: 2,
  verbose: 3,
  debug: 4,
  silly: 5
};

const logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      filename: "sys.log",
      level: "warn"
    })
  ]
});

module.exports = {
  test: () => {
    logger.info("[0" + 200 + "] - REQUEST OK ");
  },
  auth: () => {
    logger.info("[AUTH] - Authorizing Account");
  },
  auth_failed: () => {
    logger.error("Account Authentication Failed");
  },
  req_ok: (_status_code) => {
    logger.info("[0" + _status_code + "] - REQUEST OK ");
  },
  auth_req: (_message) => {
    logger.warn("[AUTH] - Account Authorization Request " + _message);
  },
  start: () => {
    logger.warn("[STRT] - " + moment().format("YYYY/MM/DD - HH:mm:ss"));
  },
  gather: () => {
    logger.info("[DATA] - GATHERING");
  },
  send: (timestamp) => {
    logger.warn("[SEND] - SYSTEM DATA SENT - ", timestamp);
  },
  reset: (timestamp) => {
    logger.warn("[REST] - RESETTING DATA STORE - ", timestamp);
  },
  time: (_time) => {
    logger.info("[TIME] - " + _time);
  },
  err: (_err) => {
    logger.error("[ERRO] - " + _time + " ms");
  },
  error: (_err) => {
    logger.error("[ERRO] - " + _err);
  },
  down: (_proc) => {
    logger.warn("[MISS] - PROCESS DOWN: " + String(_proc).toUpperCase());
  },
  notified: (_proc) => {
    logger.warn("[SEND] - NOTIFICATION OF MISSING PROCESS");
  }
};
