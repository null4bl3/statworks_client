# Statworks

Statworks is a proactive information-as-a-service ( IAS ) module.

The module will feed system statistics, of the device the module is running on, to our servers.
Here we will keep track of the status of your system, sending you warning notifications when your device reaches any critical levels you have determined. Analyse the gathered data to provide you with weekly or monthly status reports and allow you to monitor your system for running processes and notify you and your team if any process is no longer running as expected.

The module is intended to be running in its own process in order to be able to also monitor more framework specific process watch application and not to be brought down along and applications in use. At the momnet the module has only been developed for systems running Linux and BSD, but we are working on also supporting Microsoft Windows at some point.

To install either clone the repository:

```
npm install -g statworks
```

```
• init
Initializes the application on the device that you want to monitor. The application prompts you for a client token and an account key. Example: statworks init

• start
Starts the application as a background daemon.

• stop
Stops the application background daemon.

• watch
Runs the application attached to the process.

• reset
Resets the configuration object.

• log
Prints the most recent application logs to the console.
```
