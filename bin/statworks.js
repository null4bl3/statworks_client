#!/usr/bin/env node

(async () => {
  // WE NEED TO MOVE STOP INIT AND SIMILAR HOOKS INTO THIS GLOBAL FILE
  const shell = require("shelljs");
  const path = require("path");
  const chalk = require("chalk");
  const { spawn } = require("child_process");

  let path_run = path.join(__dirname, "../");
  let fs = require("fs");
  let dir = "/home/" + require("os").userInfo().username + "/.statworks";
  process.env.PATH = dir;
  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
  }
  let user_path = "/home/" + require("os").userInfo().username + "/.statworks/";
  let run_cmd =
    path_run +
    "node_modules/forever/bin/forever start -a -l " +
    user_path +
    "statworks.log -o " +
    user_path +
    "statworks_out.log -e " +
    user_path +
    "statworks_err.log " +
    path_run +
    "lib/srv.js";
  let start_cmd =
    path_run +
    "node_modules/forever/bin/forever start -a -l " +
    user_path +
    "statworks.log -o " +
    user_path +
    "statworks_out.log -e " +
    user_path +
    "statworks_err.log " +
    path_run +
    "lib/srv.js";
  let stop_cmd =
    path_run +
    "node_modules/forever/bin/forever stop " +
    path_run +
    "lib/srv.js";
  let cmd_out = "cat " + user_path + "/statworks.log | tail -n 50";
  let cmd_err = "cat " + user_path + "/statworks_err.log | tail -n 50";
  let write_logs = async (_path) => {
    const util = require("util");
    const exec = util.promisify(require("child_process").exec);
    const { stdout, stderr } = await exec(_path);
    if (stderr) {
      console.log("LOG ERROR!");
      console.log(stderr);
    } else {
      return stdout;
    }
  };
  let check_for_proc = async (_proc) => {
    return new Promise((resolve, reject) => {
      const ls = spawn("ps", ["-fC", _proc]);
      ls.stdout.on("data", (data) => {
        if (data.toString().includes(_proc)) {
          resolve(1);
        } else {
          resolve(0);
        }
      });
      ls.stderr.on("data", (data) => {
        lib.clog.error(data);
        reject(data);
      });
    });
  };

  switch (process.argv[2]) {
    case "watch":
      require("../lib/srv.js");
      break;

    case "run":
      shell.exec(run_cmd);
      console.log("STATWORKS STARTED ..");
      break;

    case "start":
      shell.exec(start_cmd);
      console.log("\n" + chalk.green(" => ") + " STATWORKS STARTED .." + "\n");
      break;

    case "stop":
      shell.exec(stop_cmd);
      console.log("\n" + chalk.red(" => ") + " STATWORKS STOPPED .. " + "\n");
      break;

    case "reset":
      break;

    case "init":
      require("../lib/srv.js");
      break;

    case "log":
      shell.echo("\n\nRUNTIME LOG:");
      shell.echo("-------------------------------------------------------");
      let tmp_out = await write_logs(cmd_out);
      console.log(tmp_out);
      shell.echo("\n\nERROR LOG:");
      shell.echo("-------------------------------------------------------");
      let tmp_err = await write_logs(cmd_err);
      console.log(tmp_err);
      process.exit(0);
      break;

    case "check":
      console.log(chalk.yellow("\n[TEST] ") + " - LOOKING UP PROCESS \n");
      // let check_for_proc = async (_proc) => {
      //   return new Promise((resolve, reject) => {
      //     const ls = spawn("ps", ["-fC", _proc]);
      //     ls.stdout.on("data", (data) => {
      //       if (data.toString().includes(_proc)) {
      //         resolve(1);
      //       } else {
      //         resolve(0);
      //       }
      //     });
      //     ls.stderr.on("data", (data) => {
      //       lib.clog.error(data);
      //       reject(data);
      //     });
      //   });
      // };
      if (process.argv[3]) {
        try {
          let run_test = await check_for_proc(process.argv[3]);
          if (run_test) {
            console.log(
              chalk.green("[FIND] ") +
                "- THE PROCESS: '" +
                chalk.bold(String(process.argv[3]).toUpperCase()) +
                "' WAS FOUND RUNNING ON YOUR SYSTEM.\n"
            );
            process.exit(0);
          } else {
            console.log(
              chalk.red("[FAIL] ") +
                "- THE PROCESS: '" +
                chalk.bold(String(process.argv[3]).toUpperCase()) +
                "' WAS _NOT_ FOUND RUNNING ON YOUR SYSTEM.\n"
            );
            process.exit(0);
          }
        } catch (e) {
          console.log(e);
        }
      } else {
        console.log(
          "[ERRO] - TO SEARCH FOR RUNNING PROCESS. PLEASE PROVIDE PARAMETER TO SEARCH FOR. eg: check 'mysqld'"
        );
        process.exit(0);
      }
      break;

    default:
      require("../lib/srv.js");
  }
})();
